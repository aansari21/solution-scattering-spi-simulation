close all
if 1 % 0 to bring preloaded coordinates; 1 to create coordinates from coordinates.gro
    [atomic_number, coordinates, water] = readCoordinates('coordinates.gro',786678);
    [atomic_number_sol, coordinates_sol, ~] = readCoordinates('coordinates_solution.gro',786593);
    coordinatesAu = AuSphere(1e-9, [1 1 1]*8e-9);
    atomic_numberAu = ones(length(coordinatesAu),1)*79;
    save('lysozyme.mat');
else
    load("lysozyme.mat");
end

figure(1)
% First plot
scatter3(coordinates(water,1), coordinates(water,2), coordinates(water,3), 1, 'filled', 'MarkerFaceAlpha', 0.1);
hold on;
scatter3(coordinates(~water,1), coordinates(~water,2), coordinates(~water,3), 1, 'filled');
scatter3(coordinatesAu(:,1)*1e9, coordinatesAu(:,2)*1e9, coordinatesAu(:,3)*1e9, 1, 'filled', 'MarkerFaceAlpha', 1);

hold off;xlabel('x (nm)'); ylabel('y (nm)'); zlabel('z (nm)');
legend('Water','Lysozyme','Gold')
axis equal
%%
% figure(2)
% scatter3(coordinates_sol(:,1), coordinates_sol(:,2), coordinates_sol(:,3), 1, 'filled', 'MarkerFaceAlpha', 0.1);
% hold on;
% %scatter3(coordinates_sol(:,1), coordinates_sol(:,2), coordinates_sol(:,3), 1, 'filled');
% hold off;xlabel('x (nm)'); ylabel('y (nm)'); zlabel('z (nm)');
% axis equal


scale = 19.87672e-9; %nm
[radius1, I_average1, I_mean_std1] = generateDiffraction(coordinates(~water,:)*1e-9, atomic_number(~water) , scale);
[radius2, I_average2, I_mean_std2] = generateDiffraction(coordinates*1e-9, atomic_number, scale);
[radius4, I_average4, I_mean_std4] = generateDiffraction([coordinates*1e-9;coordinatesAu],...
    [atomic_number; atomic_numberAu], scale);
scale = 19.97243e-9; %nm
[radius3, I_average3, I_mean_std3] = generateDiffraction(coordinates_sol*1e-9, atomic_number_sol, scale);
[radius5, I_average5, I_mean_std5] = generateDiffraction([coordinates_sol*1e-9;coordinatesAu],...
    [atomic_number_sol; atomic_numberAu], scale);
%%
figure;
errorbar(radius1, I_average1, I_mean_std1, '-', 'DisplayName', '1AKI - Vacuum','CapSize',0);
hold on;
errorbar(radius2, I_average2, I_mean_std2, '-', 'DisplayName', '1AKI - Solution','CapSize',0);
errorbar(radius3, I_average3, I_mean_std3, '-', 'DisplayName', 'Solution','CapSize',0);
errorbar(radius4, I_average4, I_mean_std4, '-', 'DisplayName', '1AKI - Solution with Gold','CapSize',0);
errorbar(radius5, I_average5, I_mean_std5, '-', 'DisplayName', 'Solution with Gold','CapSize',0);
set(gca, 'YScale', 'linear');
xlim([0 3]);
xlabel('$q (\mathring{A}^{-1})$', 'Interpreter', 'latex');
ylabel('Photon per pixel');
legend('-DynamicLegend');
disp('finished radial average');
disp('=======');
yt = get(gca, 'YTick');
set(gca, 'YTick',yt, 'YTickLabel',yt*1E-12)
%set(gca, 'YTickLabel', arrayfun(@(x) sprintf('%0.2e', str2double(x)*10^-11), yticklabels, 'UniformOutput', false));