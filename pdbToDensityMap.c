#include "mex.h"
#include "constants.h"
#include "math.h"
#define B 1.0
#define vox 2 //voxel count for evaluating an atom's density map
#define qp 2
#include "omp.h"

void findIndex(double value, double spacing, double start, int M, 
               int *arr, double *pos, int n) { 
    //Ex: value = 0.024, spacing = .25 start = 0; M = 4; n = 1
    int dc = (int) floor(((value - start + spacing/2.0) / spacing)); // ex: dc = 0
    for (int i = -n; i < n+1; i++){ 
        int d = dc + i; // linear index ex: [-1 0 1]
        arr[i+n] = (d+M) % M; // circular indexing ex: [3 0 1]  
        pos[i+n] = start+spacing*d; // linear index positions ex: [-1/8 1/8 3/8]
    }
}

void quadrature(int n, double *xw, double *wt) {
    switch (n) {
        case 1:
            xw[0] = 0.0; wt[0] = 2.0;
            break;
        case 2:
            xw[0] = -0.5773502691896257; xw[1] = 0.5773502691896257;
            wt[0] = wt[1] = 1.0;
            break;
        case 3:
            xw[0] = -0.7745966692414834; xw[1] = 0.0; xw[2] = 0.7745966692414834;
            wt[0] = wt[2] = 0.5555555555555556; wt[1] = 0.8888888888888888;
            break;
        case 4:
            xw[0] = -0.8611363115940526; xw[1] = -0.3399810435848563;
            xw[2] = 0.3399810435848563; xw[3] = 0.8611363115940526;
            wt[0] = wt[3] = 0.3478548451374538; wt[1] = wt[2] = 0.6521451548625461;
            break;
        case 5:
            xw[0] = -0.9061798459386640; xw[1] = -0.5384693101056831;
            xw[2] = 0.0; xw[3] = 0.5384693101056831; xw[4] = 0.9061798459386640;
            wt[0] = wt[4] = 0.2369268850561891; wt[1] = wt[3] = 0.4786286704993665;
            wt[2] = 0.5688888888888889;
            break;
        default:
            printf("Invalid value of n");
            break;
    }
}

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // Get the input array
    double *X = mxGetPr(prhs[0]); // 1D X array
    double *Y = mxGetPr(prhs[1]); // 1D Y array
    double *Z = mxGetPr(prhs[2]); // 1D Z array
    double dx = mxGetScalar(prhs[3]); // spacing
    double *p = mxGetPr(prhs[4]); // positions of atoms
    int atcnt = mxGetM(prhs[4]); // number of atoms
    int *atom = (int *) mxGetData(prhs[5]); // atomic number of atoms
    size_t arraySize = mxGetNumberOfElements(prhs[0]);
    double xw[qp], wt[qp];
    quadrature(qp, xw, wt);
    
    // Output argument
    mwSize dims[3] = {arraySize, arraySize, arraySize};
    plhs[0] = mxCreateNumericArray(3, dims, mxDOUBLE_CLASS, mxREAL);
    double *result = mxGetPr(plhs[0]);
    // precompute atomic scaling factor
    double baseTerms[92][5];
    double expTerms[92][5];
    for (int m = 0; m < 92; m++) {
        for (int n = 0; n < 5; n++) {
            baseTerms[m][n] = aa[m][n] * pow(4 * M_PI / (bb[m][n] + B), 1.5);
            expTerms[m][n] = -4 * M_PI * M_PI / (bb[m][n] + B); }}

    // start looping over atoms
    #pragma omp parallel for
    for (int h = 0; h < atcnt; h++)
    {
        // retrive x y z positions of atom
        double x = p[h];
        double y = p[h + atcnt];
        double z = p[h + 2*atcnt]; 
        
        // find surrounding voxel indices and positions on 3D bin
        int indx[2*vox+1], indy[2*vox+1], indz[2*vox+1];
        double xpos[2*vox+1],  ypos[2*vox+1], zpos[2*vox+1];
        findIndex(x, dx, X[0], dims[0], indx, xpos, vox); 
        findIndex(y, dx, Y[0], dims[1], indy, ypos, vox);
        findIndex(z, dx, Z[0], dims[2], indz, zpos, vox);

        // next 3 nested loops to loop over xyz neightbouring voxels
        for (int i = 0; i < 2*vox+1; i++){
            for (int j = 0; j < 2*vox+1; j++){
                for (int k = 0; k < 2*vox+1; k++){
                    mwIndex voxelIndex = indz[k] * dims[1] * dims[0] + indy[j] * dims[0] + indx[i];
                    // next 3 nested loops to loop over xyz qp^3-point within voxel for gaussian quadrature integration
                    for (int u = 0; u < qp; u++) {
                        for (int v = 0; v < qp; v++) {
                            for (int w = 0; w < qp; w++) {
                                double x2 = (xpos[i] - x + .5*xw[u]*dx);
                                double y2 = (ypos[j] - y + .5*xw[v]*dx);
                                double z2 = (zpos[k] - z + .5*xw[w]*dx);
                                double r2 = (x2 * x2 + y2 * y2 + z2 * z2)*1.0e20;
                                //mexPrintf("%f\n",sqrt(r2));
                                int m = atom[h]-1;
                                //mexPrintf("%i",m);
                                for (int n = 0; n < 5; n++)
                                    result[voxelIndex] += 0.125 * wt[u]*wt[v]*wt[w]*baseTerms[m][n] * exp(expTerms[m][n] * r2);
                            }
                        }
                    }
                }
            }
        }
    }
}
/*printf("%e %e %e startpts\n",X[0],Y[0],Z[0]);
mexPrintf("Point %d: x = %.2e, y = %.2e, z = %.2e\n", h + 1, x, y, z);
for (int var = 0; var < vox; var++)
mexPrintf("Index:\t%i\t%i\t%i\n", indx[var], indy[var], indz[var]);*/
