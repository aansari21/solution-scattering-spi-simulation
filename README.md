# Solution Scattering SPI Simulation
run.sh is based on a basic GROMACS solvation tutorial: http://www.mdtutorials.com/gmx/lysozyme/01_pdb2gmx.html


The last time-step data 'coordinates.gro' is fed into MATLAB after a few minor changes

**pbdToDensityMap.c** is a c-subroutine that converts atomic positions into density map using atomic form factors in real space populating 512^3 periodic voxel. 

Since the voxel is periodic, it can be processed using FFT very easily for simulating diffraction.

## Results
### MD Simulation in water after t = 1000ps
Number density: 3mg/mL

![MD simulation](results/figure7.png)

### Protien only scattering

![SPI scattering](results/figure6.png)

### Solution scattering without Protien
Water ring clear from the O-O pair distance in water
![Solution scattering without protien](results/figure3.png)

### Solution scattering with Protien
Water ring clear from the O-O pair distance in water
![Solution scattering with protien](results/figure5.png)

### Solution scattering with Gold
Water ring clear from the O-O pair distance in water
![Solution scattering with Gold](results/figure2.png)

### Solution scattering with Gold and protien
Water ring clear from the O-O pair distance in water
![Solution scattering](results/figure4.png)

### SAXS patterns for all
![Solution scattering](results/figure1.png)

### SAXS Simulation

![SAXS](results/figure1.png)

