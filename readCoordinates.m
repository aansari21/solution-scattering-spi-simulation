function [atomic_number, coordinates, water] = readCoordinates(filename, Natoms)
if nargin==0
    filename = 'coordinates.gro';
    Natoms = 786678;
end

% Open the file for reading
fileID = fopen(filename, 'r');

% Initialize arrays to store the extracted data
element = zeros(Natoms,1);
coordinates = zeros(Natoms,3);
water = false(Natoms,1);

i = 0;
% Read the file line by line
while ~feof(fileID)
    i = i + 1;
    if mod(i, 10000) == 0
        fprintf('%.2f%% complete\n', i / Natoms * 100);
    end
    % Read one line
    line = fgetl(fileID);
    str = strsplit(line,' ');
    % Assuming your cell array is named 'myCellArray'
    empty_indices = cellfun(@isempty, str); % Find indices of empty elements
    str(empty_indices) = []; % Delete empty elements
    % Extract the first character of the second column as a character
    char_value = str{2}(1); % Assuming the format is fixed
    if length(str{2})>1
        if strcmp(str{2}(1:2),'OW') || strcmp(str{2}(1:2),'HW')
            water(i,1) = true;
        end
    end
    % Extract the last three columns as floats
    float_values = sscanf(line(end-22:end), '%f%f%f');

    % Append the extracted data to the arrays
    element(i) = char_value;
    coordinates(i,:) = float_values';



end

% Close the file
fclose(fileID);
atomic_number = convert_to_atomic_numbers(char(element));

end