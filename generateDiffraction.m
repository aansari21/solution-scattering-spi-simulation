function [radius, I_average, I_mean_std] = generateDiffraction(coordinates2, atomic_number2,scale)
Zs = 1;
lambda = 0.177e-9; %nm
N = 512;
xi = (linspace(0,1,N+1))*scale;
x1 = 0.5*(xi(1:end-1)+xi(2:end));
dx = (xi(2) - xi(1));
y1 = x1;
z1 = x1;
%%
tic;
electronDensity =  pdbToDensityMap(x1, y1, z1, dx, coordinates2, int32(atomic_number2));
fprintf("Voxel assembly in %f s\n",toc);%%

% Normalize the kernel

tic;
farfieldintensity = fftn(electronDensity); fprintf("FFT in %f s\n",toc);
farfieldintensity = fftshift(real(farfieldintensity.*conj(farfieldintensity))); %clear electronDensity
farfieldintensity(N/2+1,N/2+1,N/2+1) = nan; % zero the origin in inverse space

% Display diffraction approximation

SZ=N;
FrequencySampling=1/(SZ*dx); %units of 1/m
FGridX=(0:SZ-1)-SZ/2;
FGridY=FGridX;
FX=FGridX*FrequencySampling;
FY=FGridY*FrequencySampling;
Xs=FX*lambda*Zs;
Ys=FY*lambda*Zs;
qx = 4*pi*sin(0.5*atan(Xs/Zs))/(lambda)*1e-10;
qy = 4*pi*sin(0.5*atan(Ys/Zs))/(lambda)*1e-10;
[QX,QY] = ndgrid(qx,qy);
figure;
surf(QX,QY,(farfieldintensity(:,:,N/2+1)),'edgealpha',.01,'EdgeColor','w');
colormap("hot")
axis([min(qx) max(qx) min(qy) max(qy)])
view([0 90])
xlabel('$q_x (\mathring{A}^{-1})$', 'Interpreter', 'latex');
ylabel('$q_y (\mathring{A}^{-1})$', 'Interpreter', 'latex');
title(sprintf('Plane-wave diffraction simulation from $\\lambda = %1.2f$ nm',...
    1e9*lambda), 'Interpreter', 'latex');
grid on
%% Find Radial profile
%Find radial coordinates
R = sqrt(qx'.^2+qy.^2+permute(qx.^2,[3 1 2])); % radius of 3d q
%frame = farfieldintensity; %(:,:,N/2);

mask = farfieldintensity <  1e+15;
num_bins = 250;
maxr = max(R,[],'all');
rbins = linspace(0,maxr,num_bins+1);

%histogram radius
[~,~,binindex] = histcounts(R,rbins);
% sum of intensity
I_sum = zeros(1,num_bins);
I_var = zeros(1,num_bins);
r_pixel_count = zeros(1, num_bins);
for i = 1:numel(farfieldintensity)
    if mask(i)
        I_sum(binindex(i)) = I_sum(binindex(i)) + farfieldintensity(i);
        r_pixel_count(binindex(i)) = r_pixel_count(binindex(i)) + 1;
    end
end

% Average of radial bins
I_average = I_sum./r_pixel_count;

for i = 1:numel(farfieldintensity)
    if mask(i)
        I_var(binindex(i)) = I_var(binindex(i)) + (I_average(binindex(i))-farfieldintensity(i))^2;
    end
end
I_mean_std = sqrt(I_var)./r_pixel_count;
radius = 0.5*(rbins(1:end-1) + rbins(2:end));