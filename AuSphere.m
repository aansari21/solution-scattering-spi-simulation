function valid_coordinates = AuSphere(diam, center)
% Define lattice constant (assuming it's 1 for simplicity)
a = 4.065e-10; % in meters

% Number of unit cells along each axis
n_units = 15;

% Initialize array to store coordinates
coordinates = zeros(n_units^3*4, 3);

% Generate coordinates for each unit cell
index = 1;
for i = 0:n_units-1
    for j = 0:n_units-1
        for k = 0:n_units-1
            % Coordinates of the corner atoms
            coordinates(index,:) = [i*a, j*a, k*a];
            coordinates(index+1,:) = [(i+0.5)*a, (j+0.5)*a, k*a];
            coordinates(index+2,:) = [(i+0.5)*a, j*a, (k+0.5)*a];
            coordinates(index+3,:) = [i*a, (j+0.5)*a, (k+0.5)*a];
            index = index + 4;
        end
    end
end
coordinates = coordinates - mean(coordinates);
distances = sqrt(sum(coordinates.^2, 2));

% Eliminate points outside radius of 2 nm
valid_indices = distances <= diam/2;
valid_coordinates = coordinates(valid_indices, :);
[Q,R] = qr(rand(3,3));
valid_coordinates = valid_coordinates*Q + center;

% Plotting
% scatter3(valid_coordinates(:,1), valid_coordinates(:,2), valid_coordinates(:,3), 'filled');
% xlabel('X (m)');
% ylabel('Y (m)');
% zlabel('Z (m)');
% axis equal
% title('Coordinates of Gold Atoms within 2nm radius in a 10x10x10 fcc Crystal Structure');