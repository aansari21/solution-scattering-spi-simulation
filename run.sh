grep -v HOH 1aki.pdb > 1AKI_clean.pdb
gmx pdb2gmx -f 1AKI_clean.pdb -o 1AKI_processed.gro -water spce
#gmx editconf -f 1AKI_processed.gro -o 1AKI_newbox.gro -c -d 9.0 -bt cubic
gmx editconf -f 1AKI_processed.gro -o 1AKI_newbox.gro -c -box 20.0 20.0 20.0 -bt cubic

gmx solvate -cp 1AKI_newbox.gro -cs spc216.gro -o 1AKI_solv.gro -p topol.top
gmx grompp -f ions.mdp -c 1AKI_solv.gro -p topol.top -o ions.tpr
gmx genion -s ions.tpr -o 1AKI_solv_ions.gro -p topol.top -pname NA -nname CL -neutral #13

gmx grompp -f minim.mdp -c 1AKI_solv_ions.gro -p topol.top -o em.tpr
gmx mdrun -deffnm em -nb gpu
gmx energy -f em.edr -o potential.xvg

gmx grompp -f nvt.mdp -c em.gro -r em.gro -p topol.top -o nvt.tpr
gmx mdrun -deffnm nvt -nb gpu 
gmx energy -f nvt.edr -o temperature.xvg

gmx grompp -f npt.mdp -c nvt.gro -r nvt.gro -t nvt.cpt -p topol.top -o npt.tpr
gmx mdrun -deffnm npt -nb gpu 
gmx energy -f npt.edr -o density.xvg

gmx grompp -f md.mdp -c npt.gro -t npt.cpt -p topol.top -o md_0_1.tpr
gmx mdrun -deffnm md_0_1 -nb gpu 
gmx trjconv -s md_0_1.tpr -f md_0_1.xtc  -o coordinates.gro